var express = require('express');
var router = express.Router();
//let User = require('../models/user.model').User;
let userController = require('../controllers/user.controller');


/* GET users listing. */
router.post('/add', userController.saveUser);

router.get('/add',  userController.addUser);
router.get('/all',  userController.getAllUsers);


router.post('/login',userController.loginUser);
router.get('/login',userController.loadLogin);
//router.get('/logout')

router.get('/:userId', userController.getUser);
router.get('/delete/:userId', userController.deleteUsers);
//router.get('/delete/:userId', userController.deleteUsers);
// router.get('/all', function (req, res, next) {
//   User.find(function (err, users) { //user find is for fetching all data from mongoBD
//     if (err) return next(err);
//     let myData = {
//       list: users
//     };
//     res.render('pages/userall.ejs',myData);
//   });
// });
// router.delete('/delete/:userId', function (req, res, next) {
//   res.send('respond with all users');
// });
module.exports = router;