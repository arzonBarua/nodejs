let User = require('../models/user.model').User;
let TokenService = require('../services/TokenService');

/**
 * render add user form
 * @param {*} req 
 * @param {*} res 
 */
let addUser = function (req, res) {
    res.render('pages/user.ejs');
};
/**
 * save news users
 * @param {*} req 
 * @param {*} res 
 */
let saveUser = function (req, res) {
    console.log(req.body);
    let userObject = new User(req.body);

    userObject.setPassword(req.body.password).then(function () {
        userObject.save().then(saveduser => {
                res.send(saveduser);
            })
            .catch(error => {
                console.log(error);
                res.status(500).send({
                    errorMessage: 'There was an error'
                });
            })
    })
};

let getAllUsers = function (req, res) {
    User.find().then(users => {
        res.render('pages/userall.ejs', {
            list: users
        });
    }).catch(error => {
        console.log(error);
        res.status(500).send({
            errorMessage: 'There was an error'
        });
    });
};


let getUser = function (req, res) {
    User.findById(req.params.userId).then(user => {
        res.render('pages/edituser.ejs', {
            user: user
        });
    }).catch(error => {
        console.log(error);
        res.status(500).send({
            errorMessage: 'There was an error'
        });
    });
};

let editUser = function (req, res) {
    User.findById(req.params.userId).then(user => {
        res.render('pages/edituser.ejs', {
            user: user
        });
    }).catch(error => {
        console.log(error);
        res.status(500).send({
            errorMessage: 'There was an error'
        });
    });
};


let deleteUsers = function (req, res) {
    User.findByIdAndRemove(req.params.userId).then(users => {
        res.redirect('/users/all');
    }).catch(error => {
        console.log(error);
        res.status(500).send({
            errorMessage: 'There was an error'
        });
    });
}

let loadLogin = function (req, res) {
    res.render('pages/login.ejs');
};


let loginUser = function (req, res, next) {
    var authenticate = User.authenticate();
    console.log(req.body);
    authenticate(req.body.username, req.body.password, function (error, user) {
        console.log(error, user);
        if (user) {
            let token =  TokenService.generateToken({user_id: user._id.toString()});
            res.send({token: token});
            //res.cookie('loggedIn', true);
            //res.redirect('/sample/vue');
        } else {
            res.status(403).send({message: 'Unauthorized'});
            //res.redirect('/users/login');
        }
    });
};

// let editUsersPost = function(req, res){
//     User.findByIdAndUpdate(req.params.userId).then(users => {
//         es.redirect('/users/all');
//     });
// };

// let editUser = function(req, res){

// }

module.exports.addUser = addUser;
module.exports.saveUser = saveUser;
module.exports.getAllUsers = getAllUsers;
module.exports.getUser = getUser;
module.exports.editUser = editUser;
module.exports.deleteUsers = deleteUsers;
module.exports.loginUser = loginUser;
module.exports.loadLogin = loadLogin;