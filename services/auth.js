let User = require('../models/user.model').User;
let TokenService = require('../services/TokenService');
module.exports.authorize = function (app, passport, LocalStrategy) {
  passport.use(new LocalStrategy(User.authenticate()));
  passport.serializeUser(User.serializeUser());
  passport.deserializeUser(User.deserializeUser());

//   passport.deserializeUser(function (id, done){
//     User.findOne({
//         _id: id
//     },'-hash','-salt'
//     });
//   });
};


// module.exports.isAuthenticated = function (req, res, next) {
//   if(req.cookies.loggedIn){
//     next();
//   } else {
//     res.redirect('/users/login');
//   }
// };
module.exports.isAuthenticated = function (req, res, next) {
  if(req.headers && req.headers.authorization){
    let token = req.headers.authorization;
    let data  = TokenService.verifyToken(token);
    
    if(data){
      next();
    }else{
      res.status(403).send({message: 'unauthorized'});
      //res.redirect('users/login');
    }
  } else {
    //res.status(403).send({message: 'unauthorized'});
    res.redirect('/users/login');
  }
};