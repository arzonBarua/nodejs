let jwt = require('jsonwebtoken');
//let Promise = require('bluebird'); //extented version of promise
let key = 'nodeKey';

/**
 * Generates token
 * @param {Object} payload contains user model
 * @param {Number} days
 */
let generateToken = function (payload, days = 1) {
  let token = jwt.sign(payload, key, {expiresIn: 60 * 60 * 24 * days});
  return token;
};

/**
 * verify toekn
 * @param {jsonwebtoken} token
 * @returns {*}
 */
let verifyConfirmToken = function (token) {
  try {
    let data = jwt.verify(token, key);
    return data;
    //return Promise.resolve(data);
  } catch (err) {
    console.log(err);
    return false;
    //return Promise.reject(err);
  }
};

module.exports.verifyConfirmToken = verifyConfirmToken;
module.exports.generateToken = generateToken;
