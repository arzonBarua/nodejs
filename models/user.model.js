const passportLocalMongoose = require('passport-local-mongoose'); //require passport plugin
let mongoose = require('mongoose');
let Schema = mongoose.Schema;
ObjectId = Schema.ObjectId;


let userSchema = new Schema({
    username: {
        type: String,
        unique: true
    },
    firstname: {
        type: String
    },
    lastname: {
        type: String
    },
    email: {
        type: String,
        unique: true
    },
    hash:{
        type: String
    },
    salt:{
        type: String
    },
    mobileNumber: {
        type: String
    }
});

userSchema.plugin(passportLocalMongoose); //add passport plugin

userSchema.statics.findUserById = function (userID){
    return this.findOne({_id: userID}).exec();
};

let User = mongoose.model('user', userSchema);
module.exports.User = User;