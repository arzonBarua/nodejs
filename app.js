var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
let localStrategy = require('passport-local').Strategy; //for passport
let passport = require('passport'); // for passport
let auth = require('./services/auth');
//let session  = require('express-session');

//var indexRouter = require('./routes/index');
//var usersRouter = require('./routes/users');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

//for passport
app.use(passport.initialize());
app.use(passport.session());


//app.use('/users', usersRouter);



// error handler
app.use(function(err, req, res, next) {
  console.log(err);
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

auth.authorize(app, passport, localStrategy);
module.exports = app;
module.exports.passport = passport; // bind passport module


require('./routes/index')(app);
// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

